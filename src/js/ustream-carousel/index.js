function useCarousel() {
  return ({

    // Settings
    config: {},
    currentTranslateX: 0,
    items: [],
    items_counts: null,
    slide_current: null,
    slide_counts: null,
    slide_width: null,
    slide_x: null,
    slide_controls: null,

    // Service variables
    pos_init_x: null,
    pos_current_x: null,
    pos_result_x: null,

    // Init
    init(options) {
      const instance = this;

      // Default config
      const _options = {
        container: '',
        per_page: 4,
        per_slide: 4,
        slide_offset: 12,
        slide_overflow: 0,
        start_position: 1,
        infinite: false,
        pos_threshold: 0.03,
        transition: {
          duration: 0.4,
          fn: 'ease-in-out'
        }
      };

      // Merge user options with default config
      const config = {
        ..._options,
        ...options
      };

      this.slide_current = config.start_position || 1; // Set start slide

      // Set common config
      this.config = {
        ...this.config,
        ...config
      };

      const user_container = $(`.${config.container}`);
      const childs = user_container.children();

      this.items_counts = childs.length;
      this.slide_counts = Math.ceil(childs.length / this.config.per_slide);

      const slider = this.utils.createSlider();
      const list = this.utils.createSliderList();
      const controls = this.utils.createSliderControls();

      $(slider).appendTo(user_container); // Slider Container
      $(list).appendTo(slider); // Slider List
      instance.slide_controls = $(controls.all()).appendTo(slider); // Slider Controls

      childs.each(function() {
        // Replace user slides in to List
        const item = instance.utils.createSlideItem();
        $(this).appendTo(item);
        $(item).appendTo(list);
        // Set instance items
        instance.items = [
          ...instance.items,
          item
        ];
      });

      this.setGrid(list);
      this.setTransition();
      this.moveToStartPosition();
      this.onActionStart();
      this.onAction();
      this.onActionEnd();
      this.onControlsClick();
    },

    // Events
    onActionStart() {
      const instance = this;
      const events = ['touchstart', 'mousedown'];
      events.forEach(eventType => {
        $('.u-carousel').on(eventType, function(e) {
          const event = instance.utils.getEvent(e);
          if (!instance.utils.isControlClick(event)) {
            $('.u-carousel').css({ cursor: 'move' });
            instance.pos_init_x = instance.pos_current_x = event.clientX;
          }
        });
      });
    },
    onAction() {
      const instance = this;
      const events = ['touchmove', 'mousemove'];
      events.forEach(eventType => {
        $('.u-carousel').on(eventType, function(e) {
          const event = instance.utils.getEvent(e);
          if (!instance.utils.isControlClick(event)) {
            instance.pos_current_x = event.clientX;
          }
        });
      });
    },
    onActionEnd() {
      const instance = this;
      const threshold = this.utils.getWidth(this.items) * this.config.pos_threshold;
      const events = ['touchend', 'mouseup'];
      events.forEach(eventType => {
        $('.u-carousel').on(eventType, function(e) {
          const event = instance.utils.getEvent(e);
          if (!instance.utils.isControlClick(event)) {
            $(this).css({ cursor: 'auto' });
            instance.pos_result_x = instance.pos_init_x - instance.pos_current_x;
            instance.pos_result_x >= threshold ? instance.nextSlide() : instance.prevSlide();
          }
        });
      });
    },
    onControlsClick() {
      const instance = this;
      this.slide_controls.each(function() {
        if (this.className.includes('prev')) {
          $(this).on('click', () => instance.prevSlide());
          return;
        }
        if (this.className.includes('next')) {
          $(this).on('click', () => instance.nextSlide());
          return;
        }
      });
    },

    // Methods
    isFirstSlide() {
      return !this.infinite && this.slide_current == 1;
    },
    isLastSlide() {
      return !this.infinite && this.slide_current == this.slide_counts;
    },
    setGrid(list) {
      $(list).css({
        gridTemplateColumns: `
          repeat(
            ${this.items_counts},
            calc(100% /
              ${this.config.per_page} - ${this.config.slide_offset}px - ${this.config.slide_overflow}px +
                ${this.config.slide_overflow == 0 ? this.config.slide_offset / this.config.per_page : 0}px)
          )`,
        gridGap: `0 ${this.config.slide_offset}px`
      });
    },
    setTransition() {
      this.items.forEach(item => {
        item.style.transition = `transform ${this.config.transition.duration}s ${this.config.transition.fn}`;
      });
    },
    setTranslateX(direction) {
      const widthItem = this.utils.getWidth(this.items);
      const offset = (widthItem + this.config.slide_offset) * this.config.per_slide;
      if (direction == 'back') {
        this.currentTranslateX = this.currentTranslateX + offset;
      }
      if (direction == 'foward') {
        this.currentTranslateX = this.currentTranslateX - offset;
      }
    },
    setSlide(direction, value=null) {
      if (value) {
        this.slide_current = value;
        return;
      }
      if (direction == 'prev') {
        this.slide_current -= 1;
      }
      if (direction == 'next') {
        this.slide_current += 1;
      }
      return this;
    },
    moveToStartPosition() {
      if (this.slide_counts < this.config.start_position || this.config.start_position <= 0) return;
      setTimeout(() => {
        const instance = this;
        const widthItem = instance.utils.getWidth(instance.items);
        instance.currentTranslateX = instance.currentTranslateX - (widthItem + instance.config.slide_offset) * (instance.config.start_position - 1);
        instance.setSlide(null, instance.config.start_position);
        $(instance.items).each(function() {
          instance.move(this);
        });
      }, 500);
    },
    move(item) {
      $(item).css({
        transform: `translateX(${ this.currentTranslateX }px)`
      });
    },

    prevSlide() {
      if (this.isFirstSlide()) return;
      this.setSlide('prev').setTranslateX('back');
      this.items.forEach((item) => this.move(item));
    },
    nextSlide() {
      if (this.isLastSlide()) return;
      this.setSlide('next').setTranslateX('foward');
      this.items.forEach((item) => this.move(item));
    },

    // Utils
    utils: {
      isControlClick(e) {
        return $(e.target).hasClass('u-carousel__nav');
      },
      getEvent(e) {
        return e.type.search('touch') !== -1 ? e.touches[0] : e;
      },
      getWidth(elem) {
        return $(elem).width();
      },
      createElem(tag, opts={}) {
        const elem = document.createElement(tag);
        Object.keys(opts).forEach(key => {
          elem[key] = opts[key];
        });
        return elem;
      },
      createSlider(opts={}) {
        return this.createElem('section', {
          className: 'u-carousel',
          ...opts
        });
      },
      createSliderList(opts={}) {
        return this.createElem('ul', {
          className: 'u-carousel__list',
          ...opts
        });
      },
      createSlideItem(opts={}) {
        return this.createElem('li', {
          className: 'u-carousel__item',
          ...opts
        });
      },
      createSliderControls(opts={prev:{}, next:{}}) {
        return ({
          prev: this.createElem('span', {
            className: 'u-carousel__nav u-carousel__prev u-carousel__nav',
            ...opts.prev
          }),
          next: this.createElem('span', {
            className: 'u-carousel__nav u-carousel__next u-carousel__nav',
            ...opts.next
          }),
          all() {
            return ([
              this.prev,
              this.next
            ]);
          }
        });
      },
    },
  });
}

export default useCarousel;
