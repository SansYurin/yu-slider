const CommonConfig = require('./webpack.common');
const { merge } = require('webpack-merge');
const { devOptions } = require('./settings');

module.exports = merge(CommonConfig, {
  devtool: 'inline-source-map',
  mode: 'development',
  devServer: devOptions
});