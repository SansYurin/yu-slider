require('dotenv-defaults').config();

const path = require('path');
const { getSource } = require('../utils');

const isDevMode = process.env.NODE_ENV === 'development';

const srcParams = {
  MAIN: {
    nameModule: 'main',
    base: path.join(process.cwd(), 'src'),
    dist: path.join(process.cwd(), 'dist'),
    static: path.join(process.cwd(), 'src/static'),
    pug: './src/views/pages',
    scss: './scss/index.scss',
    js: './js/index.js',
    img: './src/static/images',
    fonts: './src/static/fonts'
  }
};

const regExp = {
  js: /.*\.js$/,
  scss: /.*\.(sa|sc|c)ss$/,
  pug: /.*\.pug$/,
  img: /.*\.(jpg|jpeg|png|svg|gif)$/i,
  fonts: /.*\.(eot|svg|ttf|otf|woff|woff2)$/i
};

const srcStore = new Map([
  ['main', srcParams.MAIN]
]);

const src = getSource(
  srcStore,
  isDevMode,
  process.env.BUILD_MODE
);

const devOptions = {
  publicPath: '/',
  contentBase: src.static,
  watchContentBase: true,
  port: process.env.DEV_PORT,
  host: process.env.DEV_HOST,
  hot: true,
  compress: true,
  open: false,
  openPage: '/',
  allowedHosts: ['*'],
  overlay: {
    warnings: false,
    errors: true
  },
};

const pugGlobals = {
  BUILD_MODE: process.env.BUILD_MODE,
  PROJECT_NAME: 'Pug Layout'
};

const pugBeautifyOptions = {
  indent_size: 2,
  indent_char:  ' ',
  indent_with_tabs: false,
  editorconfig: false,
  eol: '\n',
  end_with_newline: false,
  indent_level: 0,
  preserve_newlines: true,
  max_preserve_newlines: 2,
  space_in_paren: false,
  space_in_empty_paren: false,
  jslint_happy: false,
  space_after_anon_function: false,
  space_after_named_function: false,
  brace_style: 'collapse',
  unindent_chained_methods: false,
  break_chained_methods: false,
  keep_array_indentation: false,
  unescape_strings: false,
  wrap_line_length: 0,
  e4x: false,
  comma_first: false,
  operator_position: 'before-newline',
  indent_empty_lines: false,
  templating: ['auto']
};

module.exports = {
  src,
  regExp,
  devOptions,
  pugGlobals,
  pugBeautifyOptions,
  isDevMode
};