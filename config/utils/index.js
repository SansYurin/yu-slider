const fs = require('fs');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const getSource = (store, isDevMode, buildMode) =>
  store.has(buildMode) && !isDevMode
    ? store.get(buildMode) : store.get('main');

const outputFiles = (target) => (url, resourcePath, context) => {
  if (target && resourcePath.includes('node_modules')) {
    return `${target}/${url}`;
  }
  return resourcePath.replace(`${context}/static`, '');
};

const getEntriesOfPlugin = (arrayOfPluginInstance) =>
  arrayOfPluginInstance.map(pluginInstance => {
    return pluginInstance.userOptions.template;
  });

const buildPug = (pugTemplate, path) => {
  const baseContext = path.replace(/\.\/src\//, '');
  const pugContext = path.replace(/\.\/src\/views\/pages/, '');
  const filename = `${pugContext ? `.${pugContext}/` : './'}${pugTemplate.replace(/\.pug/,'.html')}`;
  const template = `./${baseContext}/${pugTemplate}`;
  return (
    new HtmlWebpackPlugin({ filename, template })
  );
};

const assemblyPugFiles = (path, context) =>
  fs.readdirSync(path)
    .reduce((res, pug) => {
      const file = `${path}/${pug}`;
      const stat = fs.statSync(file);
      if (stat && stat.isDirectory()) {
        return ([
          ...res,
          ...assemblyPugFiles(file, pug)
        ]);
      } else {
        return ([
          ...res,
          buildPug(pug, path)
        ]);
      }
    }, []);


const assemblyFiles = (path, pattern) =>
  fs.readdirSync(path)
    .reduce((res, i) => {
      const file = `${path}/${i}`;
      const stat = fs.statSync(file);
      if (stat && stat.isDirectory()) {
        return ([
          ...res,
          ...assemblyFiles(file, pattern)
        ]);
      } else {
        return ([
          ...res,
          file.replace('src/', '')
        ]);
      }
    }, [])
    .filter(file => file.match(pattern));

module.exports = {
  getSource,
  outputFiles,
  buildPug,
  assemblyPugFiles,
  assemblyFiles,
  getEntriesOfPlugin
};