const ESLintPlugin = require('eslint-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BeautifyHtmlWebpackPlugin = require('beautify-html-webpack-plugin');
const { ProvidePlugin, DefinePlugin } = require('webpack');
const { src, regExp, isDevMode, pugGlobals, pugBeautifyOptions } = require('./settings');
const { assemblyPugFiles, assemblyFiles, getEntriesOfPlugin } = require('./utils');

const babel = {
  test: regExp.js,
  exclude: /node_modules/,
  use: [
    'babel-loader',
  ]
};

const pug = {
  test: regExp.pug,
  use: [{
    loader: 'pug-global-loader',
    options: {
      globalVariable: {
        namespace: 'GLOBAL',
        variables: pugGlobals
      }
    },
  }],
};

const scss = {
  test: regExp.scss,
  use: [
    isDevMode ?
      'style-loader' : MiniCssExtractPlugin.loader,
    'css-loader',
    'postcss-loader',
    'resolve-url-loader',
    {
      loader: 'sass-loader',
      options: {
        implementation: require('sass'),
        sassOptions: {
          fiber: require('fibers'),
        },
      },
    },
  ],
};

const url = {
  test: regExp.img,
  exclude: [ /fonts/, ],
  type: 'asset/resource',
  generator: {
    filename: 'images/[name][ext]'
  },
};

const font = {
  test: regExp.fonts,
  exclude: [ /images/, ],
  type: 'asset/resource',
  generator: {
    filename: 'fonts/[name][ext]'
  },
};

const pugHTMLs = assemblyPugFiles(src.pug, false);
const images = assemblyFiles(src.img, regExp.img);
const fonts = assemblyFiles(src.fonts, regExp.fonts);

const pugEntries = getEntriesOfPlugin(pugHTMLs);

module.exports = {
  context: src.base,
  entry: {
    main: [
      src.js,
      src.scss,
      ...pugEntries,
      ...images,
      ...fonts
    ],
  },
  output: {
    publicPath: '/',
    path: src.dist,
    filename: '[name].bundle.[contenthash].js',
  },
  module: {
    rules: [babel, pug, scss, url, font]
  },
  plugins: [
    new ESLintPlugin(),
    new DefinePlugin({
      BUILD_MODE: JSON.stringify(process.env.BUILD_MODE),
    }),
    new ProvidePlugin({
      $: 'jquery/dist/jquery.min.js',
      jQuery: 'jquery/dist/jquery.min.js',
      'window.jQuery': 'jquery/dist/jquery.min.js'
    }),
    new MiniCssExtractPlugin({
      filename: '[name].[contenthash].css',
    }),
    ...pugHTMLs,
    new BeautifyHtmlWebpackPlugin(pugBeautifyOptions)
  ],
  resolve: {
    extensions: ['.js', '.scss', '.pug', 'html'],
    modules: [src.base, 'node_modules'],
  },
};