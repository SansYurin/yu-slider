const CommonConfig = require('./webpack.common');
const { merge } = require('webpack-merge');
const { src } = require('./settings');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = merge(CommonConfig, {
  mode: 'production',
  plugins: [
    new CleanWebpackPlugin({
      cleanOnceBeforeBuildPatterns: [
        src.dist
      ]
    }),
  ],
});